<?php

/* Домашнее задание к лекции 3.1 «Классы и объекты»

1. Распишите своё понимание инкапсуляции. Представьте, что вас спрашивают на собеседовании.
2. Сформулируйте своими словами в чём плюсы объектов, а в чём минусы?
3. Опишите 5 классов и создайте по 2 объекта каждого класса — Машина, Телевизор, Шариковая ручка, Утка, Товар.
    Классы должны содержать свойства и методы. Все в одном файле.

Дополнительное задание:

1. Создайте класс новостей для сайта.
2. Реализуйте страницу, на которой вы будете эти новости выводить, используя только методы класса (к свойствам обращаться нельзя).
3. (Необязательно) Если окажется слишком просто, реализуйте класс комментариев и создайте метод getComments в классе новостей,
    в который будете передавать объект с комментариями, который будет также их выводить.
*/

/* Ответ своими словами:

1. Инкапсуляция - объединение в одно целое данных/структуры хранения и методов работы с ними.
    Объединение свойств/описаний какого-либо объекта и возможных действий объекта или с объектом

2. Плюсы исползования ООП:
    а) Понятней код (алгоритмы становятся ближе к тому как люди привыкли воспринимать мир)
    б) Меньше возможность допустить ошибку (можно скрыть или ограничить доступ к определенным свойствам или методам)
   Минусы:
    а) Алгоритм как правило более многословнен, чем в процедурном программировании (хотя эта многословность, как раз и делает его более понятным).
    б) Необходимо знать и уметь применять принципы ООП и синтаксические конструкции языка, предназначенные для данных целей.
    в) Итоговый код в общем случае будет занимать больше места и требовать больше ресурсов для выполения.

3. :
*/

class Car // Машина
{
    private $type;
    private $maker;
    private $color = 'Белый';
    private $speed = 0;

    public function __construct($type, $maker)
    {
        $this->type = $type;
        $this->maker = $maker;
    }

    public function drive($speed)
    {
        $this->speed = $speed;
    }

    public function stop()
    {
        $this->speed = 0;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
    }

    public function getColor(): string
    {
        return $this->color;
    }
}

class Television // Телевизор
{
    private $diagonal;
    private $type;
    private $maker;
    private $work = false;

    public function __construct($type, $maker, $diagonal)
    {
        $this->type = $type;
        $this->maker = $maker;
        $this->diagonal = $diagonal;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function on()
    {
        $this->work = true;
    }

    public function off()
    {
        $this->work = false;
    }
}

class Pen // Ручка
{
    private $color;
    private $automatic;

    public function __construct($automatic = false, $color = 'Синий')
    {
        $this->automatic = (boolean)$automatic;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function replaceRod($color)
    {
        $this->color = $color;
    }
}

class Duck // Утка
{
    private $gender;
    private $act = 'Выглядит как утка';
    private $kind;
    public $name;
    public $birthday;

    public function __construct($gender, $kind, $birthday = null)
    {
        $this->gender = $gender;
        $this->kind = $kind;
        $this->birthday = $birthday == null ? time() : $birthday;
    }

    public function quack()
    {
        $this->act = 'Крякает как утка';
        echo 'Кря-кря';
    }

    public function go()
    {
        $this->act = 'Ходит как утка';
    }

    public function sleep()
    {
        $this->act = 'Спит как утка';
    }

    public function fly()
    {
        $this->act = 'Летит как утка';
    }

    public function swim()
    {
        $this->act = 'Плывет как утка';
    }

    public function look()
    {
        return $this->act;
    }
}

class Product // Товар
{
    private $name;
    private $maker;
    public $category;
    private $price = 0;
    private $weight;
    private $discount = 0;

    public function __construct($name, $maker, $price, $weight = null)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->price = $price;
        $this->weight = $weight;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCost()
    {
        return round($this->price * (1 - $this->discount / 100), -1);
    }

    public function showProduct()
    {
        echo $this->name . ' стоит: ' . $this->getCost();
    }
}

$car1 = new Car('Грузовая', 'Камаз');
$car2 = new Car('Легковая', 'АвтоВАЗ');
$car1->setColor('Красная');
$car2->drive(60);

$tv1 = new Television('ЖК', 'Samsung', 19);
$tv2 = new Television('Плазма', 'Sony', 52);
$tv2->on();

$pen1 = new Pen();
$pen1->replaceRod('Синий');
$pen2 = new Pen(true, 'Зеленый');

$duck1 = new Duck('М', 'Нырок');
$duck1->quack();
$duck1->fly();
echo '<br>';
$duck2 = new Duck('Ж', 'Морянка');
$duck2->sleep();

$product1 = new Product('Смартфон Xiaomi Redmi Note 5A Prime 3/32GB', 'Xiaomi', 10990, 0.153);
$product1->setDiscount(26);
$product1->showProduct();
echo '<br>';

$product2 = new Product('Микроволновая печь Horizont 20MW800-1378', 'Horizont', 3190, 10.9);
$product2->setDiscount(8);
$product2->showProduct();
echo '<br>';


// Дополнительное задание
class Comment
{
    private $content = '';
    private $author = '';
    private $date = '';

    public function __construct($content, $author)
    {
        $this->content = $content;
        $this->author = $author;
        $this->date = time();
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getDate()
    {
        return $this->date;
    }
}

class News
{
    private $title = 'Заголовок';
    private $content = 'Содержание новости';
    private $tags = [];
    private $date;
    private $comments = [];
    private $author = '';

    public function __construct($title, $content, $author, $tags)
    {
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
        if (is_array($tags)) {
            $this->tags = $tags;
        } elseif (!empty($tags)) {
            $this->tags[] = (string)$tags;
        }
        $this->date = time();
    }

    public function addTag($tag)
    {
        $this->tags[] = $tag;
    }

    public function showNews()
    {
        echo "<h3>{$this->title}</h3>";
        echo '<ul style="list-style-position: inside; margin: 0 20px; padding: 0;">';
        foreach ($this->tags as $tag) {
            echo '<li style="display: inline; list-style-type: none;"><a href="#">' . $tag . '</a></li>';
        }
        echo '</ul>';
        echo "<p>{$this->content}</p>";
    }

    public function getComments(Comment $comment)
    {
        $this->comments[] = $comment;
        echo '<p style="margin-left: 60px">';
        echo '<b>' . $comment->getAuthor() . '</b> ' . date("Y-m-d H:i:s", $comment->getDate()) . '<br>';
        echo '<i>' . $comment->getContent() . '</i>';
        echo '</p>';
    }
}

$lastNews = [];
$lastNews[] = new News('Итальянца арестовали за взвешивание гашиша в супермаркете',
    'В продуктовом отделе одного из супермаркетов Рима полицией был арестован 23-летний местный житель, который на весах, предназначенных для овощей взвешивал гашиш, общим весом 94 грамма.',
    'Alex Webber', 'Криминал');
$lastNews[] = new News('Британец сдался в полицию из-за неудачного фото в объявлении о розыске',
    '35-летний британец Уэйн Эсмонд (Wayne Esmonde) был настолько возмущён фото, которое использовали в Департаменте полиции Южного Уэльса в объявлении о его розыске, что сам пришёл и сдался только чтобы мерзкий снимок удалили из интернета.',
    'Alex Webber', 'Криминал');
$lastNews[] = new News('В Папуа – Новой Гвинее колдуны сорвали парламентские выборы',
    'В Папуа – Новой Гвинее разгорелся нешуточный скандал: сразу двое политиков из провинции Восточный Сепик потребовали пересчёта голосов на парламентских выборах, обвиняя местных колдунов в краже бюллетеней из урн для голосования.',
    'Alex Webber', 'Политика');
$lastNews[] = new News('В Зимбабве крокодилы съели пастора ходившего по воде',
    'Вероятно, одним из реальных претендентов на премию Дарвина в этом году станет пастор по имени Джонатан Меттва (Jonathan Mthethwa) из Зимбабве, которого съели крокодилы во время демонстрации оным библейского чуда хождения по воде.',
    'Alex Webber', 'Религия');

foreach ($lastNews as $news) {
    $news->showNews();
}

$news = new News('Китаец целый год бесплатно обедал в ресторане по одному авиабилету',
    'Китаец Вонг Ва Ют купил билет на самолёт и целый год бесплатно обедал в реторане аэропорта Сиань города Шэньси пока его хитроумный план не был раскрыт сотрудниками авиакомпании China Eastern Airlines.',
    'Alex Webber', 'Транспорт');
$news->showNews();
$news->getComments(new Comment('Молодец парень. Я бы ему руку пожал от всего сердца :)', 'Вацлав Пионтек'));
$lastNews[] = $news;

$news = new News('Полиция Канады провела спецоперацию по задержанию детей укравших конфеты',
    'Полиция Канады провела крупномасштабную спецоперацию с привлечением спецназа, вертолёта с тепловизором, несколько кинологов с собаками и всё ради того, чтобы обнаружить и задержать подростков, укравших в магазине горсть конфет.',
    'Alex Webber', 'Криминал');
$news->showNews();
$news->getComments(new Comment('Ещё бы бюджет озвучили этой операции, во что налогоплательщикам обошлось "поймать и пожурить" похитителей нескольких конфет', 'Babuin'));
$news->getComments(new Comment('Хорошо не в штатах дело было, а то расстреляли бы детишек из пулемётов прямо из вертолёта, приняв конфеты в их руках. за оружие', 'konik'));
$lastNews[] = $news;
